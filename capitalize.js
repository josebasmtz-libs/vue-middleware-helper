const { $string } = require('jis')

function capitalize(entry)
{
    if ($string(entry))
    {
        return entry.charAt(0).toUpperCase() + entry.slice(1);
    }

    return null;
}

module.exports = capitalize;