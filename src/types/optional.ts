
type Optional<T=unknown> = T|null;

export default Optional;
