import MiddlewareHelper from "./middlewares";
import BaseMiddleware from "./middlewares/abstracts/BaseMiddleware";

const version = '1.0.0';

const exportable = {
    version,
    MiddlewareHelper,
    BaseMiddleware
}

export default exportable;