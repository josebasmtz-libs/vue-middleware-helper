import {NavigationGuard, NavigationGuardNext, Route, RouteConfig} from "vue-router";
import jis from 'jis';
import BaseMiddleware from "../middlewares/abstracts/BaseMiddleware";
import Optional from "../types/optional";

class MiddlewareHelper
{
    public static concat(...middlewares: NavigationGuard[]): NavigationGuard
    {
        return async function (to: Route, from: Route, next: NavigationGuardNext)
        {
            for(let middleware of middlewares)
            {
                let response = await (
                    new Promise(
                        (resolve) => {
                            middleware(to, from, resolve)
                        }
                    )
                );

                if (response !== undefined)
                {
                    next(response);
                    return;
                }
            }

            next();
        }
    }

    public static setBeforeEnter(routes: RouteConfig[]|RouteConfig, before: Optional<NavigationGuard> = null)
    {
        if (!routes)
        {
            return;
        }

        if (jis.$array(routes))
        {
            for(let route of (routes as RouteConfig[]))
            {
                this.setBeforeEnter(route, before);
            }
            return;
        }

        let route = (routes as RouteConfig);
        let middlewares: NavigationGuard[] = [
            this.fromMiddleware(before),
            this.getMiddlewareFromRoute(route)
        ];

        let middleware = this.fromMiddleware(middlewares);

        let children = route.children;
        if (jis.$array(children) && children.length > 0) {
            this.setBeforeEnter(children, middleware);
            return;
        }

        if (middleware)
        {
            route.beforeEnter = middleware;
        }
    }

    public static getMiddlewareFromRoute(route: RouteConfig|Route)
    {
        let middlewares = route.meta?.middleware;
        return this.fromMiddleware(middlewares);
    }

    public static fromMiddleware(middleware: any): Optional<NavigationGuard>
    {
        if (jis.$function(middleware)) {
            return middleware;
        }

        if (middleware instanceof BaseMiddleware)
        {
            return middleware.handle;
        }

        if (jis.$array(middleware))
        {
            let allowed = [];
            for (let middleware_item of middleware)
            {
                let mid = this.fromMiddleware(middleware_item);
                if (mid !== undefined)
                {
                    allowed.push(mid);
                }
            }

            if (allowed.length > 0)
            {
                return this.concat(...allowed);
            }
        }

        return undefined;
    }
}

export default MiddlewareHelper;
