
import {NavigationGuardNext, Route} from "vue-router";

abstract class BaseMiddleware {

    constructor() {
        if (this.constructor === BaseMiddleware) {
            throw new Error('Abstract classes are not instantiable');
        }
    }

    abstract handle(to: Route, from: Route, next: NavigationGuardNext);

}

export default BaseMiddleware;
