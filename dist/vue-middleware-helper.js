(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory();
	else if(typeof define === 'function' && define.amd)
		define("VueMiddlewareHelper", [], factory);
	else if(typeof exports === 'object')
		exports["VueMiddlewareHelper"] = factory();
	else
		root["VueMiddlewareHelper"] = factory();
})(this, function() {
return /******/ (() => { // webpackBootstrap
/******/ 	var __webpack_modules__ = ({

/***/ 911:
/***/ (function(module) {

(function webpackUniversalModuleDefinition(root, factory) {
	if(true)
		module.exports = factory();
	else {}
})(this, function() {
return /******/ (() => { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ({

/***/ 138:
/***/ ((__unused_webpack_module, exports) => {


Object.defineProperty(exports, "__esModule", ({ value: true }));
var Is = /** @class */ (function () {
    function Is() {
    }
    Is.callToString = function (arg) {
        return Object.prototype.toString.call(arg);
    };
    Is.$string = function (arg) {
        return Is.callToString(arg) === '[object String]';
    };
    Is.is = function (arg, type) {
        if (Is.$function(type)) {
            return arg instanceof type;
        }
        if (Is.$string(type)) {
            return Is.callToString(arg) === "[object " + type + "]";
        }
        return arg === type;
    };
    Is.$array = function (arg) {
        return Is.is(arg, 'Array');
    };
    Is.$null = function (arg) {
        return Is.is(arg, 'Null');
    };
    Is.$number = function (arg) {
        return Is.is(arg, 'Number');
    };
    Is.$object = function (arg) {
        return Is.is(arg, 'Object');
    };
    Is.$undefined = function (arg) {
        return Is.is(arg, 'Undefined');
    };
    Is.$boolean = function (arg) {
        return Is.is(arg, 'Boolean');
    };
    Is.$function = function (arg) {
        return Is.callToString(arg) === '[object Function]';
    };
    Is.objectIsValid = function (data, rules) {
        if (!Is.$object(data))
            throw new Error('The data parameter must be an Object');
        if (!Is.$object(rules))
            throw new Error('The rules parameter must be an Object');
        var $response = true;
        var $keys = Object.getOwnPropertyNames(rules);
        $keys.forEach(function (key) {
            var rule = rules[key];
            if (Is.$array(rule)) {
                if (rule.length < 1)
                    return;
                var parcial_1 = false;
                rule.forEach(function (_rule) {
                    parcial_1 = parcial_1 || Is.is(data[key], _rule);
                });
                return $response = $response && parcial_1;
            }
            $response = $response && Is.is(data[key], rule);
        });
        return $response;
    };
    Is.$numeric = function (arg) {
        if (Is.$number(arg)) {
            return true;
        }
        var $arg = String(arg || '');
        var regex = /^[-+]?(([0-9]+)|([0-9]*(\.[0-9]+))|([0-9]+\.))([Ee]([-+]?[0-9]+))?$/g;
        return regex.test($arg);
    };
    Is.$primitive = function (arg) {
        var validations = [
            Is.$undefined,
            Is.$null,
            Is.$boolean,
            Is.$number,
            Is.$string,
            function (arg) { return Is.is(arg, 'Symbol'); }
        ];
        return validations.some(function (item) { return item(arg); });
    };
    Is.$empty = function (arg) {
        var validations = [
            Is.$undefined,
            Is.$null,
            function (arg) { return Is.$boolean(arg) && !arg; },
            function (arg) { return Is.$number(arg) && arg === 0; },
            function (arg) { return (Is.$array(arg) || Is.$string(arg)) && (arg === "0" || arg.length === 0); }
        ];
        return validations.some(function (item) { return item(arg); });
    };
    return Is;
}());
exports["default"] = Is;


/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __nested_webpack_require_3891__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __nested_webpack_require_3891__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
var __webpack_exports__ = {};
// This entry need to be wrapped in an IIFE because it need to be isolated against other modules in the chunk.
(() => {
var exports = __webpack_exports__;
var __webpack_unused_export__;

__webpack_unused_export__ = ({ value: true });
var Is_1 = __nested_webpack_require_3891__(138);
exports["default"] = Is_1.default;

})();

__webpack_exports__ = __webpack_exports__["default"];
/******/ 	return __webpack_exports__;
/******/ })()
;
});

/***/ }),

/***/ 519:
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {

"use strict";

var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", ({ value: true }));
var middlewares_1 = __importDefault(__webpack_require__(363));
var BaseMiddleware_1 = __importDefault(__webpack_require__(264));
var version = '1.0.0';
var exportable = {
    version: version,
    MiddlewareHelper: middlewares_1.default,
    BaseMiddleware: BaseMiddleware_1.default
};
exports["default"] = exportable;


/***/ }),

/***/ 264:
/***/ ((__unused_webpack_module, exports) => {

"use strict";

Object.defineProperty(exports, "__esModule", ({ value: true }));
var BaseMiddleware = /** @class */ (function () {
    function BaseMiddleware() {
        if (this.constructor === BaseMiddleware) {
            throw new Error('Abstract classes are not instantiable');
        }
    }
    return BaseMiddleware;
}());
exports["default"] = BaseMiddleware;


/***/ }),

/***/ 363:
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {

"use strict";

var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", ({ value: true }));
var jis_1 = __importDefault(__webpack_require__(911));
var BaseMiddleware_1 = __importDefault(__webpack_require__(264));
var MiddlewareHelper = /** @class */ (function () {
    function MiddlewareHelper() {
    }
    MiddlewareHelper.concat = function () {
        var middlewares = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            middlewares[_i] = arguments[_i];
        }
        return function (to, from, next) {
            return __awaiter(this, void 0, void 0, function () {
                var _loop_1, _i, middlewares_1, middleware, state_1;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            _loop_1 = function (middleware) {
                                var response;
                                return __generator(this, function (_b) {
                                    switch (_b.label) {
                                        case 0: return [4 /*yield*/, (new Promise(function (resolve) {
                                                middleware(to, from, resolve);
                                            }))];
                                        case 1:
                                            response = _b.sent();
                                            if (response !== undefined) {
                                                next(response);
                                                return [2 /*return*/, { value: void 0 }];
                                            }
                                            return [2 /*return*/];
                                    }
                                });
                            };
                            _i = 0, middlewares_1 = middlewares;
                            _a.label = 1;
                        case 1:
                            if (!(_i < middlewares_1.length)) return [3 /*break*/, 4];
                            middleware = middlewares_1[_i];
                            return [5 /*yield**/, _loop_1(middleware)];
                        case 2:
                            state_1 = _a.sent();
                            if (typeof state_1 === "object")
                                return [2 /*return*/, state_1.value];
                            _a.label = 3;
                        case 3:
                            _i++;
                            return [3 /*break*/, 1];
                        case 4:
                            next();
                            return [2 /*return*/];
                    }
                });
            });
        };
    };
    MiddlewareHelper.setBeforeEnter = function (routes, before) {
        if (before === void 0) { before = null; }
        if (!routes) {
            return;
        }
        if (jis_1.default.$array(routes)) {
            for (var _i = 0, _a = routes; _i < _a.length; _i++) {
                var route_1 = _a[_i];
                this.setBeforeEnter(route_1, before);
            }
            return;
        }
        var route = routes;
        var middlewares = [
            this.fromMiddleware(before),
            this.getMiddlewareFromRoute(route)
        ];
        var middleware = this.fromMiddleware(middlewares);
        var children = route.children;
        if (jis_1.default.$array(children) && children.length > 0) {
            this.setBeforeEnter(children, middleware);
            return;
        }
        if (middleware) {
            route.beforeEnter = middleware;
        }
    };
    MiddlewareHelper.getMiddlewareFromRoute = function (route) {
        var _a;
        var middlewares = (_a = route.meta) === null || _a === void 0 ? void 0 : _a.middleware;
        return this.fromMiddleware(middlewares);
    };
    MiddlewareHelper.fromMiddleware = function (middleware) {
        if (jis_1.default.$function(middleware)) {
            return middleware;
        }
        if (middleware instanceof BaseMiddleware_1.default) {
            return middleware.handle;
        }
        if (jis_1.default.$array(middleware)) {
            var allowed = [];
            for (var _i = 0, middleware_1 = middleware; _i < middleware_1.length; _i++) {
                var middleware_item = middleware_1[_i];
                var mid = this.fromMiddleware(middleware_item);
                if (mid !== undefined) {
                    allowed.push(mid);
                }
            }
            if (allowed.length > 0) {
                return this.concat.apply(this, allowed);
            }
        }
        return undefined;
    };
    return MiddlewareHelper;
}());
exports["default"] = MiddlewareHelper;


/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module is referenced by other modules so it can't be inlined
/******/ 	var __webpack_exports__ = __webpack_require__(519);
/******/ 	__webpack_exports__ = __webpack_exports__["default"];
/******/ 	
/******/ 	return __webpack_exports__;
/******/ })()
;
});