const {name} = require('./package.json')

const path = require('path');
const capitalize = require("./capitalize");
const {$string} = require('jis');
const TerserPlugin = require("terser-webpack-plugin");

function toCamel(entry)
{
    if ($string(entry))
    {
        let parts = entry.split(/[_-]+/);
        parts = parts.map(value => capitalize(value))

        return parts.join('')
    }

    return entry;
}

module.exports = {
    mode: 'production',
    entry: {
        [name]: './src/main.ts',
        [`${name}.min`]: './src/main.ts'
    },
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: '[name].js',
        libraryTarget: 'umd',
        library: {
            name: toCamel(name),
            type: 'global'
        },
        umdNamedDefine: true,
        globalObject: "this",
        libraryExport: "default"
    },
    resolve: {
        extensions: ['.ts', '.tsx', '.js']
    },
    // devtool: 'source-map',
    module: {
        rules: [{
            test: /\.tsx?$/,
            loader: 'ts-loader',
            exclude: /node_modules/
        }]
    },
    optimization: {
        minimizer: [
            new TerserPlugin({
                include: /\.min\.js$/,
                extractComments: false
            })
        ]
    }
}